/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.childgaurd;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int add_children_screen_top_image=0x7f020000;
        public static final int add_children_screen_top_image_view_detail=0x7f020001;
        public static final int add_edit_btn=0x7f020002;
        public static final int add_edit_btn_select=0x7f020003;
        public static final int administer_btn=0x7f020004;
        public static final int administer_btn_select=0x7f020005;
        public static final int bg2=0x7f020006;
        public static final int blur_bg=0x7f020007;
        public static final int btn_add_child_back_selector=0x7f020008;
        public static final int btn_add_child_back_to_selector=0x7f020009;
        public static final int btn_add_child_cancel=0x7f02000a;
        public static final int btn_add_child_cancel_select=0x7f02000b;
        public static final int btn_add_child_cancel_selector=0x7f02000c;
        public static final int btn_add_child_save=0x7f02000d;
        public static final int btn_add_child_save_select=0x7f02000e;
        public static final int btn_add_child_save_selector=0x7f02000f;
        public static final int btn_add_child_view_detail_back_selector=0x7f020010;
        public static final int btn_add_edit_selector=0x7f020011;
        public static final int btn_administrative_selector=0x7f020012;
        public static final int btn_back=0x7f020013;
        public static final int btn_back_select=0x7f020014;
        public static final int btn_button_cancel_pop=0x7f020015;
        public static final int btn_button_cancel_pop_select=0x7f020016;
        public static final int btn_button_ok_pop=0x7f020017;
        public static final int btn_button_ok_pop_select=0x7f020018;
        public static final int btn_cacel_change_password_select=0x7f020019;
        public static final int btn_cancel=0x7f02001a;
        public static final int btn_cancel_change_password_=0x7f02001b;
        public static final int btn_cancel_select=0x7f02001c;
        public static final int btn_cancel_selector=0x7f02001d;
        public static final int btn_change_password_cancel_selector=0x7f02001e;
        public static final int btn_change_password_ok_selector=0x7f02001f;
        public static final int btn_change_password_parent_selector=0x7f020020;
        public static final int btn_child=0x7f020021;
        public static final int btn_child_close_selector=0x7f020022;
        public static final int btn_child_info=0x7f020023;
        public static final int btn_child_info_select=0x7f020024;
        public static final int btn_child_info_selector=0x7f020025;
        public static final int btn_child_select=0x7f020026;
        public static final int btn_child_selector=0x7f020027;
        public static final int btn_close=0x7f020028;
        public static final int btn_close_select=0x7f020029;
        public static final int btn_delete_child_registration_selector=0x7f02002a;
        public static final int btn_delete_parent_registration_selector=0x7f02002b;
        public static final int btn_inactive_selector=0x7f02002c;
        public static final int btn_login=0x7f02002d;
        public static final int btn_login_cancel=0x7f02002e;
        public static final int btn_login_change_password=0x7f02002f;
        public static final int btn_logout_child_selector=0x7f020030;
        public static final int btn_logout_parent_selector=0x7f020031;
        public static final int btn_monitor_selector=0x7f020032;
        public static final int btn_ok_change_password=0x7f020033;
        public static final int btn_ok_change_password_select=0x7f020034;
        public static final int btn_parent=0x7f020035;
        public static final int btn_parent_select=0x7f020036;
        public static final int btn_parent_selector=0x7f020037;
        public static final int btn_pop_cancel_selector=0x7f020038;
        public static final int btn_pop_ok_selector=0x7f020039;
        public static final int btn_register_cancel_selector=0x7f02003a;
        public static final int btn_save_selector=0x7f02003b;
        public static final int card=0x7f02003c;
        public static final int change_btn=0x7f02003d;
        public static final int change_btn_select=0x7f02003e;
        public static final int child_detail=0x7f02003f;
        public static final int child_detail_select=0x7f020040;
        public static final int delete_btn=0x7f020041;
        public static final int delete_btn_select=0x7f020042;
        public static final int delete_registration=0x7f020043;
        public static final int delete_registration_select=0x7f020044;
        public static final int ic_launcher=0x7f020045;
        public static final int icon_add=0x7f020046;
        public static final int icon_add_select=0x7f020047;
        public static final int inactivate_btn=0x7f020048;
        public static final int inactivate_btn_select=0x7f020049;
        public static final int inactivate_monitoring=0x7f02004a;
        public static final int inactivate_monitoring_select=0x7f02004b;
        public static final int l_cancel_btn=0x7f02004c;
        public static final int l_cancel_btn_select=0x7f02004d;
        public static final int l_login_btn=0x7f02004e;
        public static final int l_login_btn_select=0x7f02004f;
        public static final int l_ok_btn=0x7f020050;
        public static final int l_ok_btn_select=0x7f020051;
        public static final int l_request_password_btn=0x7f020052;
        public static final int l_request_password_btn_select=0x7f020053;
        public static final int log_out=0x7f020054;
        public static final int log_out_select=0x7f020055;
        public static final int login_text_field=0x7f020056;
        public static final int logo_bg=0x7f020057;
        public static final int main_view_btn_help_selector=0x7f020058;
        public static final int main_view_btn_login=0x7f020059;
        public static final int main_view_btn_login_select=0x7f02005a;
        public static final int main_view_btn_login_selector=0x7f02005b;
        public static final int main_view_btn_register=0x7f02005c;
        public static final int main_view_btn_register_select=0x7f02005d;
        public static final int main_view_btn_register_selector=0x7f02005e;
        public static final int main_view_help_btn=0x7f02005f;
        public static final int main_view_help_btn_select=0x7f020060;
        public static final int mobile_number_bar=0x7f020061;
        public static final int monitor_btn=0x7f020062;
        public static final int monitor_btn_select=0x7f020063;
        public static final int nick_name_bar=0x7f020064;
        public static final int pr_parent_bar=0x7f020065;
        public static final int pr_pr_email_bar=0x7f020066;
        public static final int r_btn_cancel=0x7f020067;
        public static final int r_btn_cancel_select=0x7f020068;
        public static final int r_btn_save=0x7f020069;
        public static final int r_btn_save_select=0x7f02006a;
        public static final int r_enter_passeord=0x7f02006b;
        public static final int r_password_bar=0x7f02006c;
        public static final int splash_screen=0x7f02006d;
        public static final int sprater=0x7f02006e;
    }
    public static final class id {
        public static final int action_settings=0x7f080049;
        public static final int bottomImage=0x7f080006;
        public static final int bottomImageViewDetail=0x7f08003f;
        public static final int btnAddChildViewDetail=0x7f080001;
        public static final int btnAddOrEditChildren=0x7f080023;
        public static final int btnAdministrative=0x7f080024;
        public static final int btnBackAddChild=0x7f08000a;
        public static final int btnCancel=0x7f08002e;
        public static final int btnCancelAddChild=0x7f08000b;
        public static final int btnCancelChild=0x7f080008;
        public static final int btnCancelParent=0x7f080048;
        public static final int btnChangePassword=0x7f080011;
        public static final int btnChangePasswordCancel=0x7f080012;
        public static final int btnDeleteParent=0x7f080047;
        public static final int btnDeleteParentRegistration=0x7f080027;
        public static final int btnInactiveMonitor=0x7f080026;
        public static final int btnLogoutParent=0x7f080028;
        public static final int btnMonitor=0x7f080025;
        public static final int btnPopCancel=0x7f080044;
        public static final int btnPopOk=0x7f080043;
        public static final int btnSaveChild=0x7f080007;
        public static final int btn_cancel=0x7f08001d;
        public static final int btn_change_password=0x7f08001e;
        public static final int btn_child_back=0x7f080017;
        public static final int btn_child_info=0x7f080014;
        public static final int btn_delete_child_registration=0x7f080015;
        public static final int btn_login=0x7f08001c;
        public static final int btn_logout_child=0x7f080016;
        public static final int editTextDialogUserInput=0x7f080042;
        public static final int etChildCode=0x7f080004;
        public static final int etChildMobileNo=0x7f080003;
        public static final int etEmail=0x7f08002f;
        public static final int etMobileNumber=0x7f080030;
        public static final int etNewPassword=0x7f08000f;
        public static final int etNewPasswordAgain=0x7f080010;
        public static final int etNickName=0x7f080002;
        public static final int etOldPassword=0x7f08000e;
        public static final int etParentName=0x7f080036;
        public static final int etPassword=0x7f08001b;
        public static final int etUserEmail=0x7f080035;
        public static final int etUserName=0x7f08001a;
        public static final int heading=0x7f08002b;
        public static final int help=0x7f080022;
        public static final int imgLogo=0x7f08003a;
        public static final int la=0x7f08002a;
        public static final int layout_child_registration=0x7f080045;
        public static final int layout_root=0x7f080040;
        public static final int listViewChildDetails=0x7f08000c;
        public static final int login=0x7f080020;
        public static final int map=0x7f080019;
        public static final int pbAddChild=0x7f080009;
        public static final int pbAddChildViewDetail=0x7f08000d;
        public static final int pbChangePassword=0x7f080013;
        public static final int pbChildMainView=0x7f080018;
        public static final int pbChildRegistration=0x7f080034;
        public static final int pbLogin=0x7f08001f;
        public static final int pbParentMainView=0x7f080029;
        public static final int pbParentRegistration=0x7f080039;
        public static final int reEnterPassword=0x7f080031;
        public static final int registerChild=0x7f080032;
        public static final int registerChildCancel=0x7f080033;
        public static final int registerParent=0x7f080037;
        public static final int registerParentCancel=0x7f080038;
        public static final int registration=0x7f080021;
        public static final int registrationChild=0x7f08002d;
        public static final int registrationParent=0x7f08002c;
        public static final int textView1=0x7f080041;
        public static final int topImage=0x7f080000;
        public static final int tvCode=0x7f08003e;
        public static final int tvHeading=0x7f08003b;
        public static final int tvMobileNumber=0x7f08003d;
        public static final int tvName=0x7f08003c;
        public static final int tvNote=0x7f080005;
        public static final int tvRandomNumber=0x7f080046;
    }
    public static final class layout {
        public static final int activity_add_child=0x7f030000;
        public static final int activity_add_child_view_detail=0x7f030001;
        public static final int activity_change_parent_password=0x7f030002;
        public static final int activity_child_main_view=0x7f030003;
        public static final int activity_google_map=0x7f030004;
        public static final int activity_login=0x7f030005;
        public static final int activity_main=0x7f030006;
        public static final int activity_parent_main_view=0x7f030007;
        public static final int activity_registration=0x7f030008;
        public static final int activity_registration_child=0x7f030009;
        public static final int activity_registration_parent=0x7f03000a;
        public static final int activity_splash=0x7f03000b;
        public static final int inflate_add_view_child_detail=0x7f03000c;
        public static final int inflate_child_popup=0x7f03000d;
        public static final int inflate_child_registration=0x7f03000e;
        public static final int inflate_first_view=0x7f03000f;
        public static final int inflate_parent_delete_registration=0x7f030010;
        public static final int prompts=0x7f030011;
    }
    public static final class menu {
        public static final int main=0x7f070000;
    }
    public static final class string {
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int hello_world=0x7f050002;
        public static final int login=0x7f050003;
        public static final int logintextpassword=0x7f050005;
        public static final int logintextusername=0x7f050004;
        public static final int mobileNumber=0x7f05000a;
        public static final int nickName=0x7f050009;
        public static final int parentName=0x7f05000f;
        public static final int password=0x7f05000b;
        public static final int reEnterPassword=0x7f05000c;
        public static final int register=0x7f05000d;
        public static final int registration=0x7f050006;
        public static final int registrationChild=0x7f050008;
        public static final int registrationParent=0x7f050007;
        public static final int username=0x7f05000e;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
