package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Activity_Add_Child extends Activity implements OnClickListener{
	EditText et_child_name,et_child_code,et_child_mobile_number;
	Button btn_save,btn_cancel,btn_child_detail;
	ProgressBar pb;
	int status;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_add_child);
	    registerwidgets();
	    setListener();
	}

	private void setListener() {
		// TODO Auto-generated method stub
		btn_cancel.setOnClickListener(this);
		btn_save.setOnClickListener(this);
		btn_child_detail.setOnClickListener(this);
	}

	private void registerwidgets() {
		// TODO Auto-generated method stub
		et_child_name = (EditText)findViewById(R.id.etNickName);
		et_child_code = (EditText)findViewById(R.id.etChildCode);
		et_child_mobile_number = (EditText)findViewById(R.id.etChildMobileNo);
		btn_save = (Button)findViewById(R.id.btnSaveChild);
		btn_cancel = (Button)findViewById(R.id.btnCancelChild);
		btn_child_detail = (Button)findViewById(R.id.btnAddChildViewDetail);
		pb = (ProgressBar)findViewById(R.id.pbAddChild);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btnSaveChild:
			btn_cancel.setEnabled(false);
			btn_save.setEnabled(false);
			if(et_child_name.length()==0){
				Utils.showAlertDialog("your user name is empty.....!",Activity_Add_Child.this);
				btn_cancel.setEnabled(true);
				btn_save.setEnabled(true);
			}else if(et_child_mobile_number.length()==0){
				Utils.showAlertDialog("your Mobile Number is empty.....!",Activity_Add_Child.this);
				btn_cancel.setEnabled(true);
				btn_save.setEnabled(true);
			}else if(et_child_code.length()==0){
				Utils.showAlertDialog("your Code is empty.....!",Activity_Add_Child.this);
				btn_cancel.setEnabled(true);
				btn_save.setEnabled(true);
			}else
				sendRequestJsonRegistration();
			break;
		case R.id.btnCancelChild:
			finish();
			break;
		case R.id.btnAddChildViewDetail:
			Intent intent_child_detail =new Intent(Activity_Add_Child.this,Activity_Add_Child_View_Detail.class);
//			registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
			startActivity(intent_child_detail);
			break;
			
		}
	}
	private void sendRequestJsonRegistration(){
		pb.setVisibility(View.VISIBLE);
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		RequestParams params = new RequestParams();
		params.put("pid",settings.getString("key", null));
	    params.put("name", et_child_name.getText().toString());
	    params.put("code", et_child_code.getText().toString());
	    params.put("mobile", et_child_mobile_number.getText().toString());
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_add_child, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Add_Child.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Add_Child.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         		
						showAlertDialog(json_obj.getString("msg"),Activity_Add_Child.this);
					}else{
						Utils.showAlertDialogOne(json_obj.getString("msg"),Activity_Add_Child.this);
					}
					btn_cancel.setEnabled(true);
					btn_save.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	private void showAlertDialog(String message,Context context){
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.inflate_first_view, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		
		final TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
		final Button btn_ok = (Button)promptsView.findViewById(R.id.btnDeleteParent);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		// set dialog message
		tv.setText(message);
		btn_ok.setOnClickListener(new OnClickListener() {

		    public void onClick(View v) {
		    	finish();
		    }
		});

		alertDialog.show();
	}
}

