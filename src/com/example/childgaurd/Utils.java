package com.example.childgaurd;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Utils {
	public static boolean isDataValid(String email,String userName, String password,String reEnterPassword,Context context) {
		if(email.length()==0){
			showAlertDialog("Your Email ID is empty.....!",context);
			return false;
		}else if(userName.length()==0){
			showAlertDialog("Your Parent Name is empty.....!",context);
			return false;
		}else if(password.length()==0){
			showAlertDialog("Your password is empty.....!",context);
			return false;
		}else if(reEnterPassword.length()==0){
			showAlertDialog("Please enter your password again.....!",context);
			return false;
		}else if(!password.equals(reEnterPassword)){
			showAlertDialog("Your both Password doesn't match.....!",context);
			return false;
		}else{	
			if(!isValidEmail(email)){
				showAlertDialog("Your email id is not correct...!",context);
				return false;
			}else{
				return true;
			}
		}
	}
	 public static void showAlertDialog(String message,Context context){
		new AlertDialog.Builder(context)
		.setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		})
		.show()
		.setCanceledOnTouchOutside(false);
	 }
	 public static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	 }
	public static boolean isDataValidForChild(String email,String userName, String password,String reEnterPassword,String mobileNumber,Context context) {
		if(email.length()==0){
			showAlertDialog("Your Email ID is empty.....!",context);
			return false;
		}else if(userName.length()==0){
			showAlertDialog("Your Nick Name is empty.....!",context);
			return false;
		}else if(mobileNumber.length()==0){
			showAlertDialog("Your Mobile Number is empty.....!",context);
			return false;
		}else if(password.length()==0){
			showAlertDialog("Your password is empty.....!",context);
			return false;
		}else if(reEnterPassword.length()==0){
			showAlertDialog("Please enter your password again.....!",context);
			return false;
		}else if(!password.equals(reEnterPassword)){
			showAlertDialog("Your both Password doesn't match.....!",context);
			return false;
		}else{	
			if(!isValidEmail(email)){
				showAlertDialog("Your email id is not correct...!",context);
				return false;
			}else{
				return true;
			}
		}
	}
	public static boolean passwordConfirmation(String password,Context context){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
	    if(settings.getString("password", null).equals(password))
	    	return true;
	    else 
	    	return false;
	}
	public static void showAlertDialogOne(String message,Context context){

		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.inflate_first_view, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		
		final TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
		final Button btn_ok = (Button)promptsView.findViewById(R.id.btnDeleteParent);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		// set dialog message
		tv.setText(message);
		btn_ok.setOnClickListener(new OnClickListener() {

		    public void onClick(View v) {
		    	alertDialog.cancel();
		    }
		});

		alertDialog.show();
	}

}
