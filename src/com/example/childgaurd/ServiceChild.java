package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class ServiceChild extends Service{
	static public Context serviceContext;
	static boolean NetworkConnection = true;
	public LocationManager locationMangaer=null;
	private LocationListener locationListener=null;	
	int status;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		serviceContext = this;
		
		locationListener = new MyLocationListener();
		locationMangaer = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		 Criteria myCriteria = new Criteria();
		 myCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		 myCriteria.setPowerRequirement(Criteria.POWER_LOW);
		 String myProvider = locationMangaer.getBestProvider(myCriteria, true); 
		 
		 if(displayGpsStatus(serviceContext))
	   			locationMangaer.requestLocationUpdates(myProvider,30000 , 0,
	   	                locationListener);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
	    locationMangaer.removeUpdates(locationListener);
	    this.stopSelf();
	    Log.d("helo", "onDestroy");

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	public Boolean displayGpsStatus(Context context) {
   		ContentResolver contentResolver = ServiceChild.serviceContext.getContentResolver();
   		boolean gpsStatus = Settings.Secure.isLocationProviderEnabled(
   				contentResolver, LocationManager.GPS_PROVIDER);
   		if (gpsStatus) {
   			return true;

   		} else {
   			return false;
   		}
	}

	private class MyLocationListener implements LocationListener {
		
		@Override
		public void onLocationChanged(Location loc) {
			Toast.makeText(ServiceChild.this,loc.getLatitude()+ " " + loc.getLongitude() , Toast.LENGTH_SHORT).show();
			updateUserLocation(""+loc.getLatitude(),""+loc.getLongitude());
			
	    }
	     	
		@Override
		public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
		}
		@Override
		public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
		}

		@Override
		public void onStatusChanged(String provider, int status,
				Bundle extras) {
			// TODO Auto-generated method stub
		}
		public void updateUserLocation(String lat, String lng){
//			Toast.makeText(getApplicationContext(),lat + lng, Toast.LENGTH_LONG).show();
//			long dtMili = System.currentTimeMillis();
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ServiceChild.this);
			RequestParams params = new RequestParams();
			params.put("cid",settings.getString("key", null));
//			params.put("time_stamp",String.valueOf(dtMili));
			params.put("lat",lat);
			params.put("lng",lng);
//	        
//	        //send post request for user login
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.get(Constant.address_url_base+Constant.address_child_location ,params, new JsonHttpResponseHandler(){  

	             @Override
	             public void onSuccess(final JSONObject object){
							try {
								status = object.getInt("status");
								if(status == 1)            		
									Toast.makeText(getApplicationContext(), object.getString("msg"), Toast.LENGTH_SHORT).show();
							} catch (JSONException e) {
//								 TODO Auto-generated catch block
								e.printStackTrace();
							}
						
	            	 

	             }
	             @Override
	             protected void handleFailureMessage(Throwable e, String responseBody) {
	 				try{
						JSONObject jobject =  new JSONObject(responseBody);
						int status = jobject.getInt("status");
						if(status ==0)
							Toast.makeText(getApplicationContext(), jobject.getString("msg"), Toast.LENGTH_SHORT).show();
//							Utils.showAlertDialog(jobject.getString("msg"), ServiceChild.this);
						
						
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}

	             }
				@Override
				public void onSuccess(int arg0, JSONArray arg1) {
					// TODO Auto-generated method stub
					try {
						JSONObject json_obj = arg1.getJSONObject(0);
						status = json_obj.getInt("status");
						if(status == 1){         
							Toast.makeText(getApplicationContext(), "location Updated", Toast.LENGTH_SHORT).show();
						}else
							Toast.makeText(getApplicationContext(), "location Not Updated", Toast.LENGTH_SHORT).show();
					} catch (JSONException e) {
//						 TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
	           });

//	    	client.get(Constant.address_url_base+Constant.address_child_location , params , new JsonHttpResponseHandler(){  
//	    		@Override
//	            public void onSuccess(final JSONObject object){
//	            	int status = 0;
//					try {
//						status = object.getInt("status");
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//	            	 if(status ==1){
//	            		 
//	            	}else{
//					
//					}
//	            }
//	    	});
    	}
			
	}
	

}
