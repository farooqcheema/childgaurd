package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Activity_Parent_Main_View extends Activity implements OnClickListener{
	Button btn_logout,btn_delete_registration,btn_change_password,btn_add_child,
			btn_administrative_origins,btn_monitor_child,btn_inactive_monitoring;
	ProgressBar pb;
	int status;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_parent_main_view);
	    registerwidgets();
	    setListener();
	}
	 private void registerwidgets(){
		 btn_logout = (Button)findViewById(R.id.btnLogoutParent);
		 btn_delete_registration = (Button)findViewById(R.id.btnDeleteParentRegistration);
		 btn_change_password = (Button)findViewById(R.id.btnChangePassword);
		 btn_add_child	= (Button)findViewById(R.id.btnAddOrEditChildren);
		 btn_administrative_origins	= (Button)findViewById(R.id.btnAdministrative);
		 btn_inactive_monitoring	= (Button)findViewById(R.id.btnInactiveMonitor);
		 btn_monitor_child	= (Button)findViewById(R.id.btnMonitor);
		 pb = (ProgressBar)findViewById(R.id.pbParentMainView);
	 }
	 private void setListener(){
		 btn_logout.setOnClickListener(this);
		 btn_delete_registration.setOnClickListener(this);
		 btn_change_password.setOnClickListener(this);
		 btn_add_child.setOnClickListener(this);
	 }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btnAddOrEditChildren:
			Intent intent_add_child =new Intent(Activity_Parent_Main_View.this,Activity_Add_Child.class);
			startActivity(intent_add_child);
			break;
		case R.id.btnLogoutParent:
			logout();
			break;	
		case R.id.btnChangePassword:
			Intent intent_change_password =new Intent(Activity_Parent_Main_View.this,Activity_Change_Parent_Password.class);
			startActivity(intent_change_password);
			break;
		case R.id.btnDeleteParentRegistration:
			confirmDeleteRegistration();
			break;
		}
	}
	
					/*Deleting Parent Registration View....... */
	private void confirmDeleteRegistration(){
		LayoutInflater li = LayoutInflater.from(Activity_Parent_Main_View.this);
		View promptsView = li.inflate(R.layout.inflate_parent_delete_registration, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Activity_Parent_Main_View.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		
//		final TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
		final Button btn_ok = (Button)promptsView.findViewById(R.id.btnDeleteParent);
		final Button btn_cancel = (Button)promptsView.findViewById(R.id.btnCancelParent);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		// set dialog message
		
		btn_ok.setOnClickListener(new OnClickListener() {

		    public void onClick(View v) {
		    	alertDialog.cancel();
		    	btn_add_child.setEnabled(false);
		    	btn_logout.setEnabled(false);
		    	btn_administrative_origins.setEnabled(false);
		    	btn_inactive_monitoring.setEnabled(false);
		    	btn_monitor_child.setEnabled(false);
		    	btn_change_password.setEnabled(false);
		    	sendRequestJsonRegistrationForParentDeleteRegistration();
		    }
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
		   public void onClick(View v) {                            
			   	alertDialog.cancel();
		   }
		});
		alertDialog.show();

	}
		/*Json Request For Delete Parent.....*/
	private void sendRequestJsonRegistrationForParentDeleteRegistration(){
		pb.setVisibility(View.VISIBLE);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		RequestParams params = new RequestParams();
	    params.put("id",settings.getString("key", null));
	    params.put("cat",settings.getString("logintype", null));
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_delete ,params, new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Parent_Main_View.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Parent_Main_View.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         
						deleteRegistration();
					}else
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_Parent_Main_View.this);
			    	btn_add_child.setEnabled(true);
			    	btn_logout.setEnabled(true);
			    	btn_administrative_origins.setEnabled(true);
			    	btn_inactive_monitoring.setEnabled(true);
			    	btn_monitor_child.setEnabled(true);
			    	btn_change_password.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
         });
	}
	
			/* Going To Registration View...*/
	private void deleteRegistration(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
 		SharedPreferences.Editor editor = settings.edit();
 		editor.putString("key",null).commit();
 		editor.putString("logintype",null).commit();
 		editor.putString("password", null).commit();
		Intent intent_registration =new Intent(Activity_Parent_Main_View.this,Activity_Registration.class);
		intent_registration.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
		startActivity(intent_registration);
	}
					/*logout Parent Method...........*/
	private void logout(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
 		SharedPreferences.Editor editor = settings.edit();
 		editor.putString("key",null).commit();
 		editor.putString("logintype",null).commit();
 		editor.putString("password", null).commit();
		Intent intent_logout =new Intent(Activity_Parent_Main_View.this,Activity_login.class);
		intent_logout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
		startActivity(intent_logout);
		finish();

	}

}
