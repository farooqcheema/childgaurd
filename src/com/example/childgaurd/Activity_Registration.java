package com.example.childgaurd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Activity_Registration extends Activity implements OnClickListener{
	Button btn_child_registration,btn_parent_registration,btn_cancel;
	RelativeLayout la;
	View v;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_registration);
	    registerwidgets();
	    setListener();
	}
    private void registerwidgets(){
    	btn_child_registration = (Button)findViewById(R.id.registrationChild);
    	btn_parent_registration = (Button)findViewById(R.id.registrationParent);
    	btn_cancel = (Button)findViewById(R.id.btnCancel);
    	la = (RelativeLayout)findViewById(R.id.la);
	}
    private void setListener(){
    	btn_child_registration.setOnClickListener(this);
    	btn_parent_registration.setOnClickListener(this);
    	btn_cancel.setOnClickListener(this);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.registrationChild:
			Intent registration_child_intent =new Intent(Activity_Registration.this,Activity_Registration_Child.class);
			startActivity(registration_child_intent);
			break;
		case R.id.registrationParent:
			Intent registration_parent_intent =new Intent(Activity_Registration.this,Activity_Registration_Parent.class);
			startActivity(registration_parent_intent);
			break;
		case R.id.btnCancel:
			finish();
			break;
		}
	}

}
