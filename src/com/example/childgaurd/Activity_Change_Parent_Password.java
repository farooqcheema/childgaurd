package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class Activity_Change_Parent_Password extends Activity implements OnClickListener{
	EditText et_old_password,et_new_password,et_again_new_password;
	Button btn_ok,btn_cancel;
	int status;
	ProgressBar pb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_change_parent_password);
	    registerwidgets();
	    setListener();
	    et_new_password.setTypeface(Typeface.DEFAULT);
		et_new_password.setTransformationMethod(new PasswordTransformationMethod());
		et_old_password.setTypeface(Typeface.DEFAULT);
		et_old_password.setTransformationMethod(new PasswordTransformationMethod());
		et_again_new_password.setTypeface(Typeface.DEFAULT);
		et_again_new_password.setTransformationMethod(new PasswordTransformationMethod());
	}

	private void setListener() {
		btn_ok.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
	}

	private void registerwidgets() {
		et_old_password = (EditText)findViewById(R.id.etOldPassword);
		et_new_password	= (EditText)findViewById(R.id.etNewPassword);
		et_again_new_password	= (EditText)findViewById(R.id.etNewPasswordAgain);
		btn_ok = (Button)findViewById(R.id.btnChangePassword);
		btn_cancel = (Button)findViewById(R.id.btnChangePasswordCancel);
		pb = (ProgressBar)findViewById(R.id.pbChangePassword);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btnChangePassword: 
			if(isPasswordValidForChild(et_old_password.getText().toString(),et_new_password.getText().toString(),et_again_new_password.getText().toString(),
					Activity_Change_Parent_Password.this)){
				pb.setVisibility(View.VISIBLE);
				btn_cancel.setEnabled(false);
				btn_ok.setEnabled(false);
				sendRequestJsonRegistration();
			}
			break;
		case R.id.btnChangePasswordCancel:
			finish();
			break;
		}
	}
	private void sendRequestJsonRegistration(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		RequestParams params = new RequestParams();
	    params.put("pid",settings.getString("key", null));
	    params.put("oldpass",settings.getString("password", null));
	    params.put("newpass",et_new_password.getText().toString());
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_change_password, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Change_Parent_Password.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Change_Parent_Password.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){    
						SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	 		    		SharedPreferences.Editor editor = settings.edit();
	 		    		editor.putString("password",et_new_password.getText().toString()).commit();
						showAlertDialog(json_obj.getString("msg"),Activity_Change_Parent_Password.this);
					}else{
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_Change_Parent_Password.this);
					}
					btn_cancel.setEnabled(true);
					btn_ok.setEnabled(true);
//					btn_cancel.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	 public void showAlertDialog(String message,Context context){
			new AlertDialog.Builder(context)
			.setMessage(message)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent registration_child_intent =new Intent(Activity_Change_Parent_Password.this,Activity_Parent_Main_View.class);
					registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
					startActivity(registration_child_intent);
				}
			})
			.show()
			.setCanceledOnTouchOutside(false);
	 }
	private boolean isPasswordValidForChild(String oldPassword,String newPassword,String reEnterPassword,Context context) {
		if(oldPassword.length()==0){
			Utils.showAlertDialog("Please enter your Current Password.....!",context);
			return false;
		}else if(!Utils.passwordConfirmation(oldPassword,Activity_Change_Parent_Password.this)){
			Utils.showAlertDialog("Your Current Password is Invalid...!",context);
			return false;
		}else if(newPassword.length()==0){
			Utils.showAlertDialog("Please enter your New Password.....!",context);
			return false;
		}else if(newPassword.length()<5||newPassword.length()>10){
			Utils.showAlertDialog("Your New Password(5-10 Characters)...!",context);
			return false;
		}else if(reEnterPassword.length()==0){
			Utils.showAlertDialog("Please enter your New Password Again.....!",context);
			return false;
		}else if(!newPassword.equals(reEnterPassword)){
			Utils.showAlertDialog("Your new password and Re-Enter password does not match.....!",context);
			return false;
		}else		
			return true;		
	}	
	private boolean matchOldPassword(){
		return true;
	}
}
