package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_login extends Activity implements OnClickListener{
	EditText et_password,et_user_email;
	Button btn_login,btn_cancel,btn_request_password;
	int status;
	ProgressBar pb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_login);
		registerwidgets();
		setListener();
		et_password.setTypeface(Typeface.DEFAULT);
		et_password.setTransformationMethod(new PasswordTransformationMethod());
	}
	 private void registerwidgets(){
		 et_password = (EditText)findViewById(R.id.etPassword);
		 et_user_email = (EditText)findViewById(R.id.etUserName);
		 btn_login = (Button)findViewById(R.id.btn_login);
		 btn_cancel = (Button)findViewById(R.id.btn_cancel);
		 btn_request_password = (Button)findViewById(R.id.btn_change_password);
		 pb = (ProgressBar)findViewById(R.id.pbLogin);
	 }
	 private void setListener(){
		 btn_login.setOnClickListener(this);
		 btn_request_password.setOnClickListener(this);
		 btn_cancel.setOnClickListener(this);
	 }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btn_login:
			if(et_user_email.length()==0)
				Utils.showAlertDialog("your user name is empty.....!",Activity_login.this);
			else if(et_password.length()==0)
				Utils.showAlertDialog("your password is empty.....!",Activity_login.this);
			else{
				btn_login.setEnabled(false);
				btn_request_password.setEnabled(false);
				btn_cancel.setEnabled(false);
				pb.setVisibility(View.VISIBLE);
				sendRequestJsonRegistration();
			}
			break;
		case R.id.btn_change_password:
			createViewEdit();
			break;
		case R.id.btn_cancel:
			finish();
			break;
			
		}
	}
	private void sendRequestJsonRegistration(){
		RequestParams params = new RequestParams();
	    params.put("password", et_password.getText().toString());
	    params.put("name", et_user_email.getText().toString());
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_login, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_login.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_login.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         		
						String loginAs = json_obj.getString("cat");	
						if(loginAs.equals("child")){
							SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 		    		SharedPreferences.Editor editor = settings.edit();
		 		    		editor.putString("key",json_obj.getString("id")).commit();
		 		    		editor.putString("logintype",json_obj.getString("cat")).commit();
		 		    		editor.putString("password",et_password.getText().toString()).commit();
							Intent registration_child_intent =new Intent(Activity_login.this,Activity_Child_Main_View.class);
							registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
							startActivity(registration_child_intent);
						}else if(loginAs.equals("parent")){
							SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 		    		SharedPreferences.Editor editor = settings.edit();
		 		    		editor.putString("key",json_obj.getString("id")).commit();
		 		    		editor.putString("logintype",json_obj.getString("cat")).commit();
		 		    		editor.putString("password",et_password.getText().toString()).commit();
							Intent registration_parent_intent =new Intent(Activity_login.this,Activity_Parent_Main_View.class);
							registration_parent_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
							startActivity(registration_parent_intent);
						}
					}else{
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_login.this);
					}
					btn_login.setEnabled(true);
					btn_request_password.setEnabled(true);
					btn_cancel.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	private void createViewEdit(){
		LayoutInflater li = LayoutInflater.from(Activity_login.this);
		View promptsView = li.inflate(R.layout.prompts, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Activity_login.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
//		alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInput);
		final TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
		final Button btn_ok = (Button)promptsView.findViewById(R.id.btnPopOk);
		final Button btn_cancel = (Button)promptsView.findViewById(R.id.btnPopCancel);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		// set dialog message
		
		btn_ok.setOnClickListener(new OnClickListener() {

		    public void onClick(View v) {
				if(!Utils.isValidEmail(userInput.getText().toString())){
					tv.setText("You have enter invalid Email, Please try again..!");
				}else{
					pb.setVisibility(View.VISIBLE);
					sendRequestJsonRegistrationRequestPasswod(userInput.getText().toString(),tv,alertDialog);
				}

		    }
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
		   public void onClick(View v) {                            
			   alertDialog.cancel();
		   }
		});
		
		
//		alertDialogBuilder
//				.setCancelable(false)
//				.setPositiveButton("Cancel",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int id) {
//								dialog.cancel();
//							}
//						})
//				.setNegativeButton("OK",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int id) {
//								if(!Utils.isValidEmail(userInput.getText().toString())){
//									Utils.showAlertDialog("An Email with your Password could not be sent to your Email Account Please check your Internet and Email Configuration.",Activity_login.this);
//								}else{
//									pb.setVisibility(View.VISIBLE);
//									sendRequestJsonRegistrationRequestPasswod(userInput.getText().toString());
//								}
//
//							}
//						});
		// create alert dialog
		
		// show it
		alertDialog.show();

	}
//	@Override
//	protected void onPrepareDialog(int id, Dialog dialog) {
//	    super.onPrepareDialog(id, dialog);
//	    AlertDialog alertDialog = (AlertDialog)dialog;
//	    Button button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
//	    button.setCompoundDrawablesWithIntrinsicBounds(this.getResources().getDrawable(
//	            R.drawable.add_edit_btn), null, null, null);
//
//	}
	private void sendRequestJsonRegistrationRequestPasswod(String email,final TextView tv,final AlertDialog alertDialog){
		RequestParams params = new RequestParams();
	    params.put("email", email);
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_request_password, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_login.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_login.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){
						alertDialog.cancel();
						Utils.showAlertDialogOne("An Email with your Password has been successfully sent to your Email Account.",Activity_login.this);
					}else if(status == 0){
						tv.setText("");
						Handler h=new Handler();
						h.postDelayed(new Runnable(){
						public void run(){
						//change your text here
							tv.setText("You have entered an invalid email, Please try again!");
							tv.setTextColor(Color.RED);
						}
						}, 50);
						
					}
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
				e.printStackTrace();
			}

			}
           });
	}
}
