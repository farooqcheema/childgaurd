package com.example.childgaurd;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Activity_Add_Child_View_Detail extends Activity implements OnClickListener{
	int status;
	ProgressBar pb;
	ArrayList<ChildData> childData;
	ListView lv;
	Button btn_back,btn_cancel;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_add_child_view_detail);
	    registerwidgets();
	    setListener();
	    sendJsonRequestLoopi();
	}

	private void setListener() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
	}

	private void registerwidgets() {
		// TODO Auto-generated method stub
		pb = (ProgressBar)findViewById(R.id.pbAddChildViewDetail);
		childData = new ArrayList<ChildData>();
		lv	=	(ListView)findViewById(R.id.listViewChildDetails);
		btn_back = (Button)findViewById(R.id.btnBackAddChild);
		btn_cancel = (Button)findViewById(R.id.btnCancelAddChild);
	}
	void sendJsonRequestLoopi(){
		pb.setVisibility(View.VISIBLE);
		RequestParams params = new RequestParams();
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
	    params.put("pid",settings.getString("key", null));
	
        
        //send get request for Child Details
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.get(Constant.address_url_base+Constant.address_child_view_detail, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
            	int status = 0;
				try {
					status = object.getInt("status");
					 if(status ==1){ 
		 					JSONArray jArray1=object.getJSONArray("msg");
		 					for(int i=0;i<jArray1.length();i++){
		 						JSONObject	jobject1 = jArray1.getJSONObject(i);
		 						ChildData child_detail = 	new ChildData();
		 						child_detail.name	= 	jobject1.getString("news_id");
		 						child_detail.mobileNumber	=	jobject1.getString("news_title");
		 						child_detail.code = jobject1.getString("news_description");
		 			             
		 						childData.add(child_detail);
		 			             
		 					}
		 					pb.setVisibility(View.INVISIBLE);
		 					ListAdapter1 listAdapter=new ListAdapter1();
		 					lv.setAdapter(listAdapter);

//		 					ListAdapter adapter = new SimpleAdapter(Activity_News_Detail.this,newslist,
//		 							R.layout.contactlist, new String[] { "news_title","date"},new int[]{R.id.category,R.id.newsdate});
//		 					setListAdapter(adapter);
					 }
				
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
            	
             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0){
//						UserInfo userInfo= UserInfo.getInstance(context);	
						Utils.showAlertDialog(jobject.getString("error_message"), Activity_Add_Child_View_Detail.this);
//						userInfo.saveUserInfo(context);
					}
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
     		@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         		
						JSONArray jArray1=json_obj.getJSONArray("msg");
	 					for(int i=0;i<jArray1.length();i++){
	 						JSONObject	jobject1 = jArray1.getJSONObject(i);
	 						ChildData child_detail = 	new ChildData();
	 						child_detail.name	= 	jobject1.getString("name");
	 						child_detail.mobileNumber	=	jobject1.getString("mobile");
	 						child_detail.code = jobject1.getString("code");
	 			             
	 						childData.add(child_detail);
	 			             
	 					}
	 					pb.setVisibility(View.INVISIBLE);
	 					ListAdapter1 listAdapter=new ListAdapter1();
	 					lv.setAdapter(listAdapter);
					}else{
						Utils.showAlertDialogOne(json_obj.getString("msg"),Activity_Add_Child_View_Detail.this);
					}
//					btn_cancel.setEnabled(true);
//					btn_save.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
    	});
    	

	}
	class ListAdapter1 extends ArrayAdapter<ChildData>{

	 	public ListAdapter1() {
	 		super(Activity_Add_Child_View_Detail.this, R.layout.inflate_add_view_child_detail,childData);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@SuppressLint("CutPasteId")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_add_view_child_detail, null);
 		    }
 			TextView tv_name = (TextView)convertView.findViewById(R.id.tvName);
 			TextView tv_heading = (TextView)convertView.findViewById(R.id.tvHeading);
 			TextView tv_mobile_number = (TextView)convertView.findViewById(R.id.tvMobileNumber);
 			TextView tv_code = (TextView)convertView.findViewById(R.id.tvCode);
 			tv_heading.setText("Child " + (position+1));
 			tv_name.setText("Name: "+childData.get(position).name);
 			tv_mobile_number.setText("Mobile Number: "+childData.get(position).mobileNumber);
 			tv_code.setText("Code: "+childData.get(position).code);
 			return convertView;	
		}
	 	

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btnBackAddChild:
			finish();
			break;
		case R.id.btnCancelAddChild:
			Intent intent_main =new Intent(Activity_Add_Child_View_Detail.this,Activity_Parent_Main_View.class);
			intent_main.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
			startActivity(intent_main);
			break;
		}
	}
}
