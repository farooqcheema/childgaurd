package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class Activity_Child_Main_View extends Activity implements OnClickListener{
	Button btn_delete_registration,btn_logout,btn_child_info,btn_child_back;
	int status;
	ProgressBar pb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_child_main_view);
	    registerwidgets();
	    setListener();
	    if(!isMyServiceRunning())
	    	startService(new Intent(this, ServiceChild.class));
	}
	 private void registerwidgets(){
		 btn_delete_registration = (Button)findViewById(R.id.btn_delete_child_registration);
		 btn_logout = (Button)findViewById(R.id.btn_logout_child);
		 btn_child_info = (Button)findViewById(R.id.btn_child_info);
		 btn_child_back = (Button)findViewById(R.id.btn_child_back);
		 pb	=	(ProgressBar)findViewById(R.id.pbChildMainView);
	 }
	 private void setListener(){
		 btn_delete_registration.setOnClickListener(this);
		 btn_logout.setOnClickListener(this);
		 btn_child_info.setOnClickListener(this);
		 btn_child_back.setOnClickListener(this);
	 }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.btn_delete_child_registration:
			showAlertDialog(Activity_Child_Main_View.this,true);
			break;
		case R.id.btn_logout_child:
			showAlertDialog(Activity_Child_Main_View.this,false);
			break;
		case R.id.btn_child_info:
			btn_child_info.setEnabled(false);
			btn_child_back.setEnabled(false);
			btn_delete_registration.setEnabled(false);
			btn_logout.setEnabled(false);
			sendRequestJsonRegistrationForChildInfo();
			break;
		case R.id.btn_child_back:
			finish();
			break;
		}
	}
	private void logout(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
 		SharedPreferences.Editor editor = settings.edit();
 		editor.putString("key",null).commit();
 		editor.putString("logintype",null).commit();
 		stopService(new Intent(Activity_Child_Main_View.this, ServiceChild.class));
		Intent intent_login =new Intent(Activity_Child_Main_View.this,Activity_login.class);
		startActivity(intent_login);
		finish();

	}
	private void deleteRegistration(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
 		SharedPreferences.Editor editor = settings.edit();
 		editor.putString("key",null).commit();
 		editor.putString("logintype",null).commit();
 		editor.putString("password", null).commit();
 		stopService(new Intent(Activity_Child_Main_View.this, ServiceChild.class));
		Intent intent_registration =new Intent(Activity_Child_Main_View.this,Activity_Registration.class);
		startActivity(intent_registration);
		finish();

	}
	private void sendRequestJsonRegistration(){
		pb.setVisibility(View.VISIBLE);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		RequestParams params = new RequestParams();
	    params.put("id",settings.getString("key", null));
	    params.put("cat",settings.getString("logintype", null));
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_delete ,params, new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Child_Main_View.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Child_Main_View.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         
						deleteRegistration();
					}else
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_Child_Main_View.this);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	private void showAlertDialog(Context context,final boolean logout){

		LayoutInflater li = LayoutInflater.from(Activity_Child_Main_View.this);
		View promptsView = li.inflate(R.layout.inflate_child_popup, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Activity_Child_Main_View.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
//		alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		final EditText userInput = (EditText) promptsView
				.findViewById(R.id.editTextDialogUserInput);
		final TextView tv = (TextView)promptsView.findViewById(R.id.textView1);
		final Button btn_ok = (Button)promptsView.findViewById(R.id.btnPopOk);
		final Button btn_cancel = (Button)promptsView.findViewById(R.id.btnPopCancel);
		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		// set dialog message
		
		btn_ok.setOnClickListener(new OnClickListener() {

		    public void onClick(View v) {
				if(!Utils.passwordConfirmation(userInput.getText().toString(),Activity_Child_Main_View.this)){
					tv.setText("You have enter invalid password, Please try again..!");
				}else if(logout){
					sendRequestJsonRegistration();
				}else if(!logout)
					logout();

		    }
		});

		btn_cancel.setOnClickListener(new OnClickListener() {
		   public void onClick(View v) {                            
			   alertDialog.cancel();
		   }
		});
		alertDialog.show();
	}
	
				/*child info request .......!*/
	private void sendRequestJsonRegistrationForChildInfo(){
		pb.setVisibility(View.VISIBLE);
		
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		RequestParams params = new RequestParams();
	    params.put("cid",settings.getString("key", null));
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_child_info ,params, new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Child_Main_View.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Child_Main_View.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){         
						Utils.showAlertDialogOne("Child Name: "+json_obj.getString("name") +"\n"+ "Code: " + json_obj.getString("code") + 
								"\n"+ "Mobile Number: "+json_obj.getString("mobile"),Activity_Child_Main_View.this);
					}else
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_Child_Main_View.this);
					btn_child_info.setEnabled(true);
					btn_child_back.setEnabled(true);
					btn_delete_registration.setEnabled(true);
					btn_logout.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	private boolean isMyServiceRunning(){
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (ServiceChild.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
}
