package com.example.childgaurd;

public class Constant{
	static String address_url_base = "http://www.swenggcosoftware.net/chg/services/";
	static String address_parent_registration = "parent_reg.php";
	static String address_login = "login.php";
	static String address_request_password = "req_pass.php";
	static String address_child_registration = "child_reg.php";
	static String address_delete = "delete_pc.php";
	static String address_change_password = "change_pass.php";
	static String address_child_info = "child_info.php";
	static String address_add_child = "pc_relation.php";
	static String address_child_view_detail = "relation_info.php";
	static String address_child_location = "locations.php";
}

