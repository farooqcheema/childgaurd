package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Activity_Registration_Child extends Activity implements OnClickListener{
	EditText et_user,et_password,et_email,et_enter_password,et_mobile_no;
	Button btn_register,btn_cancel;
	int status;
	ProgressBar pb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_registration_child);
	    registerwidgets();
	    setListener();
	}
	private void registerwidgets(){
    	btn_register = (Button)findViewById(R.id.registerChild);
    	et_email = (EditText)findViewById(R.id.etEmail);
    	et_user = (EditText)findViewById(R.id.etNickName);
    	et_mobile_no = (EditText)findViewById(R.id.etMobileNumber);
    	et_password = (EditText)findViewById(R.id.etPassword);
    	et_enter_password = (EditText)findViewById(R.id.reEnterPassword);
    	pb = (ProgressBar)findViewById(R.id.pbChildRegistration);
    	btn_cancel = (Button)findViewById(R.id.registerChildCancel);
	}
    private void setListener(){
    	btn_register.setOnClickListener(this);
    	btn_cancel.setOnClickListener(this);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.registerChild:
			btn_register.setEnabled(false);
			btn_cancel.setEnabled(false);
			if(Utils.isDataValidForChild(et_email.getText().toString(), et_user.
					getText().toString(), et_password.getText().toString(), 
						et_enter_password.getText().toString(), et_mobile_no.getText().
							toString(), Activity_Registration_Child.this)){
				pb.setVisibility(View.VISIBLE);
				sendRequestJsonRegistration();
			}else{
				btn_register.setEnabled(true);
				btn_cancel.setEnabled(true);

			}
			break;
		case R.id.registerChildCancel:
			finish();
			break;
		}		
	}
private void sendRequestJsonRegistration(){
		
		RequestParams params = new RequestParams();
	    params.put("email", et_email.getText().toString());
	    params.put("pass", et_password.getText().toString());
	    params.put("name", et_user.getText().toString());
	    params.put("mobile",et_mobile_no.getText().toString());
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_child_registration, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Registration_Child.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
            	btn_register.setEnabled(true);
				btn_cancel.setEnabled(true);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Registration_Child.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){
						popUpView(json_obj.getString("code"),json_obj.getString("id"));
					}else
						Utils.showAlertDialog(json_obj.getString("msg"), Activity_Registration_Child.this);
					btn_register.setEnabled(true);
					btn_cancel.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
           });
	}
	private void popUpView(String randomNumber,final String idChild){
		LayoutInflater li = LayoutInflater.from(Activity_Registration_Child.this);
		View promptsView = li.inflate(R.layout.inflate_child_registration, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Activity_Registration_Child.this);

		// set prompts.xml to alertdialog builder
		alertDialogBuilder.setView(promptsView);
		final TextView userInput = (TextView) promptsView
				.findViewById(R.id.tvRandomNumber);
		userInput.setText(randomNumber);
		// set dialog message
		alertDialogBuilder
				.setNegativeButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int id) {
								SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			 		    		SharedPreferences.Editor editor = settings.edit();
			 		    		editor.putString("key",idChild).commit();
			 		    		editor.putString("logintype","child").commit();
			 		    		editor.putString("password",et_password.getText().toString()).commit();
								Intent registration_child_intent =new Intent(Activity_Registration_Child.this,Activity_Child_Main_View.class);
								registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
								startActivity(registration_child_intent);
							}
						});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.show();
	}
}
