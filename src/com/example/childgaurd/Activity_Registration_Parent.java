package com.example.childgaurd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class Activity_Registration_Parent extends Activity implements OnClickListener{
	Button btn_register,btn_cancel;
	EditText et_user,et_password,et_email,et_enter_password;
	int status;
	ProgressBar pb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setContentView(R.layout.activity_registration_parent);
	    registerwidgets();
	    setListener();
	}
    private void registerwidgets(){
    	btn_register = (Button)findViewById(R.id.registerParent);
    	btn_cancel = (Button)findViewById(R.id.registerParentCancel);
    	et_user = (EditText)findViewById(R.id.etParentName);
    	et_password = (EditText)findViewById(R.id.etPassword);
    	et_email = (EditText)findViewById(R.id.etUserEmail);
    	et_enter_password = (EditText)findViewById(R.id.reEnterPassword);
    	pb = (ProgressBar)findViewById(R.id.pbParentRegistration);
	}
    private void setListener(){
    	btn_register.setOnClickListener(this);
    	btn_cancel.setOnClickListener(this);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.registerParent:
			if(Utils.isDataValid(et_email.getText().toString(),et_user.getText().toString()
					,et_password.getText().toString(),et_enter_password.getText().toString(),
					Activity_Registration_Parent.this)){
				btn_register.setEnabled(false);
				btn_cancel.setEnabled(false);
				pb.setVisibility(View.VISIBLE);
				sendRequestJsonRegistration();
			}
			break;
		case R.id.registerParentCancel:
			finish();
			break;
		}
	}
private void sendRequestJsonRegistration(){
		
		RequestParams params = new RequestParams();
	    params.put("name", et_user.getText().toString());
	    params.put("pass", et_password.getText().toString());
	    params.put("email", et_email.getText().toString());
	    
    	AsyncHttpClient client = new AsyncHttpClient();
    	client.setTimeout(60000);
    	client.get(Constant.address_url_base+Constant.address_parent_registration, params , new JsonHttpResponseHandler(){  

             @Override
             public void onSuccess(final JSONObject object){
						try {
							status = object.getInt("status");
							pb.setVisibility(View.INVISIBLE);
							if(status == 1)            		
								Utils.showAlertDialog(object.getString("msg"),Activity_Registration_Parent.this);
						} catch (JSONException e) {
//							 TODO Auto-generated catch block
							e.printStackTrace();
						}
					
            	 

             }
             @Override
             protected void handleFailureMessage(Throwable e, String responseBody) {
            	pb.setVisibility(View.INVISIBLE);
 				try{
					JSONObject jobject =  new JSONObject(responseBody);
					int status = jobject.getInt("status");
					if(status ==0)
						Utils.showAlertDialog(jobject.getString("msg"), Activity_Registration_Parent.this);
					
				} catch (JSONException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}

             }
			@Override
			public void onSuccess(int arg0, JSONArray arg1) {
				// TODO Auto-generated method stub
				pb.setVisibility(View.INVISIBLE);
				try {
					JSONObject json_obj = arg1.getJSONObject(0);
					status = json_obj.getInt("status");
					if(status == 1){SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
						SharedPreferences.Editor editor = settings.edit();
						editor.putString("key",json_obj.getString("id")).commit();
						editor.putString("logintype","parent").commit();
						editor.putString("password",et_password.getText().toString()).commit();
						Intent registration_child_intent =new Intent(Activity_Registration_Parent.this,Activity_Parent_Main_View.class);
						registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
						startActivity(registration_child_intent);
					}else 
						Utils.showAlertDialog(json_obj.getString("msg"),Activity_Registration_Parent.this);
					btn_register.setEnabled(true);
					btn_cancel.setEnabled(true);
				} catch (JSONException e) {
//					 TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
           });
	}
//	private void parentLogin(String id){
//		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
// 		SharedPreferences.Editor editor = settings.edit();
// 		editor.putString("key",id).commit();
// 		editor.putString("logintype","parent").commit();
//		Intent registration_child_intent =new Intent(Activity_Registration_Parent.this,Activity_Parent_Main_View.class);
//		registration_child_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
//		startActivity(registration_child_intent);
//	}

}
