package com.example.childgaurd;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener{
	Button btn_login,btn_register;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
    		if(settings.getString("key", null)!=null){
    			if(settings.getString("logintype", null).equals("child")){
    				Intent intent_child =new Intent(MainActivity.this,Activity_Child_Main_View.class);
    				startActivity(intent_child);
    				finish();	
    			}else if(settings.getString("logintype", null).equals("parent")){
    				Intent intent_parent =new Intent(MainActivity.this,Activity_Parent_Main_View.class);
    				startActivity(intent_parent);
    				finish();	
    			}
    	}
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        registerwidgets();
        setListener();
    }
    private void registerwidgets(){
    	btn_login = (Button)findViewById(R.id.login);
    	btn_register = (Button)findViewById(R.id.registration);
	}
    private void setListener(){
    	btn_login.setOnClickListener(this);
    	btn_register.setOnClickListener(this);
    }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.login:
			Intent login_intent =new Intent(MainActivity.this,Activity_login.class);
			startActivity(login_intent);
			break;
		case R.id.registration:
			Intent registration_intent =new Intent(MainActivity.this,Activity_Registration.class);
			startActivity(registration_intent);
			break;
		}

	}
    
}
